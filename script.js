let loan = 0;

function getSalary(){
    let salary = document.getElementById("salary");
    salary.innerHTML = 100 + parseInt(salary.innerHTML);
}

function saveSalary(){
    let salary = document.getElementById("salary");
    let balance = document.getElementById("balance");

    balance.innerHTML = parseInt(salary.innerHTML) + parseInt(balance.innerHTML);
    salary.innerHTML = 0;
}

function getLoan(){
    if (loan == 1){
        alert("You can't recive more loans!");
    } else {
        let maxLoan = parseInt(balance.innerHTML) * 2;
        let amount = Number(prompt("Enter loan amount:"));
        if (amount > maxLoan){
            alert("Rejected. You can loan no more than " + maxLoan + " kr")
        } else {
                    loan += 1;
        balance.innerHTML = amount + parseInt(balance.innerHTML);
        }
    }
}

function chooseComputer(){
    let computer = document.getElementById("computerList").value;

    switch(computer){
        case "0":
            document.getElementById("features").innerHTML = "";
            break;
        case "1":
            document.getElementById("features").innerHTML = "It has a screen<br>There's a keyboard<br>(Some keys might not work)";
            document.getElementById("price").innerHTML = "1500"
            document.getElementById("computerTitle").innerHTML = "Intol Celeroon E10";
            document.getElementById("computerInfo").innerHTML = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
            break;
        case "2":
             document.getElementById("features").innerHTML = "Can be charged<br>Bright screen<br>Dont work no more";
             document.getElementById("price").innerHTML = "500"
             document.getElementById("computerTitle").innerHTML = "Lenovo Region 5";
             document.getElementById("computerInfo").innerHTML = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
             break;
        case "3":
            document.getElementById("features").innerHTML = "Super fast<br>Super good<br>Super expensive";
            document.getElementById("price").innerHTML = "9500"
            document.getElementById("computerTitle").innerHTML = "MSI GS75 Stealth";
            document.getElementById("computerInfo").innerHTML = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
            break;
        case "4":
            document.getElementById("features").innerHTML = "Decent screen<br>Connects to internet";
            document.getElementById("price").innerHTML = "1000"
            document.getElementById("computerTitle").innerHTML = "Acer Nitro 5";
            document.getElementById("computerInfo").innerHTML = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
            break;
    }
}

function buyNow(){
    let balance = document.getElementById("balance");
    let price = document.getElementById("price");
    if (parseInt(price.innerHTML) > parseInt(balance.innerHTML)){
        alert("Denied. You dont have enough money.")
    }else{
        balance.innerHTML = parseInt(balance.innerHTML) - parseInt(price.innerHTML)
        alert("Purchase completed")
    }
    
}